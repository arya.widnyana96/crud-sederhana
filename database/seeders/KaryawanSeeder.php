<?php

namespace Database\Seeders;

use App\Models\Karyawan;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $karyawan=[
            [
                'nama' => 'Agus Juniarta',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2006-06-01'),
            ],
            [
                'nama' => 'Agung',
                'pekerjaan' => 'Penari Balet',
                'tanggal_lahir'=> Carbon::parse('2008-06-21'),
            ],
            [
                'nama' => 'Ayu Wulan',
                'pekerjaan' => 'Admin',
                'tanggal_lahir'=> Carbon::parse('2004-10-12'),
            ],
            [
                'nama' => 'Agus Sapta',
                'pekerjaan' => 'Programmer',
                'tanggal_lahir'=> Carbon::parse('1996-09-06'),
            ],
            [
                'nama' => 'Agung Sukro',
                'pekerjaan' => 'Pemadam Kebakaran',
                'tanggal_lahir'=> Carbon::parse('1999-09-29'),
            ],
            [
                'nama' => 'Cristian Sitofu',
                'pekerjaan' => 'CEO',
                'tanggal_lahir'=> Carbon::parse('2000-04-20'),
            ],
            [
                'nama' => 'Bento Sugas',
                'pekerjaan' => 'Tukang Bangunan',
                'tanggal_lahir'=> Carbon::parse('2005-08-01'),
            ],
            [
                'nama' => 'Danto Sunanrto',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2002-06-11'),
            ],
            [
                'nama' => 'Asan Kamil',
                'pekerjaan' => 'Pejabat Desa',
                'tanggal_lahir'=> Carbon::parse('2003-10-13'),
            ],
            [
                'nama' => 'Jainal',
                'pekerjaan' => 'Pendaki',
                'tanggal_lahir'=> Carbon::parse('2001-03-09'),
            ],
            [
                'nama' => 'Samsun Akdil',
                'pekerjaan' => 'Penceramah',
                'tanggal_lahir'=> Carbon::parse('2008-08-30'),
            ],
            [
                'nama' => 'Karta Atmasara',
                'pekerjaan' => 'Guru',
                'tanggal_lahir'=> Carbon::parse('2000-05-24'),
            ],
            [
                'nama' => 'Agus Guntur',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2006-06-01'),
            ],
            [
                'nama' => 'Agus Karta',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2000-09-23'),
            ],
            [
                'nama' => 'Lisa',
                'pekerjaan' => 'Dancer',
                'tanggal_lahir'=> Carbon::parse('1997-06-20'),
            ],
            [
                'nama' => 'Irene',
                'pekerjaan' => 'Penyanyi',
                'tanggal_lahir'=> Carbon::parse('1990-05-05'),
            ],
            [
                'nama' => 'Chou Tzuyu',
                'pekerjaan' => 'Penyani',
                'tanggal_lahir'=> Carbon::parse('1999-06-14'),
            ],
            [
                'nama' => 'Anang',
                'pekerjaan' => 'Artis',
                'tanggal_lahir'=> Carbon::parse('1986-10-01'),
            ],
            [
                'nama' => 'Uus Dahlan',
                'pekerjaan' => 'Pelawak',
                'tanggal_lahir'=> Carbon::parse('1882-12-01'),
            ],
            [
                'nama' => 'Agung Siwon',
                'pekerjaan' => 'Artis',
                'tanggal_lahir'=> Carbon::parse('1986-09-22'),
            ],
            [
                'nama' => 'Eko Suryawan',
                'pekerjaan' => 'Pelawak',
                'tanggal_lahir'=> Carbon::parse('1990-05-27'),
            ],
            [
                'nama' => 'Fitri Karlinawati',
                'pekerjaan' => 'Tukang Jahit',
                'tanggal_lahir'=> Carbon::parse('1988-01-05'),
            ],
            [
                'nama' => 'Surya Insomnia',
                'pekerjaan' => 'Pelawak',
                'tanggal_lahir'=> Carbon::parse('1990-06-20'),
            ],
            [
                'nama' => 'Andi Syamsun',
                'pekerjaan' => 'Buruh',
                'tanggal_lahir'=> Carbon::parse('2001-02-03'),
            ],
            [
                'nama' => 'Agus Lanang Suendra',
                'pekerjaan' => 'Pelajar',
                'tanggal_lahir'=> Carbon::parse('2006-07-30'),
            ],
            [
                'nama' => 'Santi Handayani',
                'pekerjaan' => 'Karyawan swasta',
                'tanggal_lahir'=> Carbon::parse('2002-08-09'),
            ],
            [
                'nama' => 'Arya Widnyana',
                'pekerjaan' => 'Teknisi',
                'tanggal_lahir'=> Carbon::parse('1996-07-28'),
            ],
            [
                'nama' => 'Made Ayu',
                'pekerjaan' => 'Penari',
                'tanggal_lahir'=> Carbon::parse('1999-08-30'),
            ],
            [
                'nama' => 'Mantana Fitri',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2005-07-01'),
            ],
            [
                'nama' => 'Cinta Laura Larasati',
                'pekerjaan' => 'Artis',
                'tanggal_lahir'=> Carbon::parse('1993-03-08'),
            ],
            [
                'nama' => 'Anak Agung Cahya',
                'pekerjaan' => 'Penari',
                'tanggal_lahir'=> Carbon::parse('2005-06-06'),
            ],
            [
                'nama' => 'Agus Wiryatama',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2004-04-08'),
            ],
            [
                'nama' => 'Parno',
                'pekerjaan' => 'Buruh',
                'tanggal_lahir'=> Carbon::parse('1990-06-23'),
            ],
            [
                'nama' => 'Kim Jihyo',
                'pekerjaan' => 'Artis',
                'tanggal_lahir'=> Carbon::parse('1996-04-24'),
            ],
            [
                'nama' => 'Ayu Diartini',
                'pekerjaan' => 'Pedagang',
                'tanggal_lahir'=> Carbon::parse('1995-11-01'),
            ],
            [
                'nama' => 'Bagas Santosa',
                'pekerjaan' => 'Programmer',
                'tanggal_lahir'=> Carbon::parse('2000-09-21'),
            ],
            [
                'nama' => 'Agus Adi',
                'pekerjaan' => 'Teknisi',
                'tanggal_lahir'=> Carbon::parse('2001-08-19'),
            ],
            [
                'nama' => 'Anang Hendara',
                'pekerjaan' => 'Karyawan Swasta',
                'tanggal_lahir'=> Carbon::parse('2003-06-01'),
            ],
            [
                'nama' => 'Bento Setia',
                'pekerjaan' => 'Buruh',
                'tanggal_lahir'=> Carbon::parse('1990-06-26'),
            ],
            [
                'nama' => 'Agus Sento',
                'pekerjaan' => 'Buruh',
                'tanggal_lahir'=> Carbon::parse('2002-06-01'),
            ],
            [
                'nama' => 'Wulandari Ayu',
                'pekerjaan' => 'Karyawan Swata',
                'tanggal_lahir'=> Carbon::parse('2000-03-08'),
            ],
            [
                'nama' => 'Erik Santosa',
                'pekerjaan' => 'Pegawai Bank',
                'tanggal_lahir'=> Carbon::parse('1998-06-09'),
            ],
            [
                'nama' => 'Agus Sinar',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2003-08-11'),
            ],
            [
                'nama' => 'Agus Marno',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2002-07-02'),
            ],
            [
                'nama' => 'Agus Herman',
                'pekerjaan' => 'Pelukis',
                'tanggal_lahir'=> Carbon::parse('2006-06-01'),
            ],
            [
                'nama' => 'Samsun',
                'pekerjaan' => 'Paranormal',
                'tanggal_lahir'=> Carbon::parse('1990-08-21'),
            ],
            [
                'nama' => 'Ewan Laras',
                'pekerjaan' => 'Karyawan swata',
                'tanggal_lahir'=> Carbon::parse('1997-07-19'),
            ],
            [
                'nama' => 'Agus Cahyo',
                'pekerjaan' => 'Guru',
                'tanggal_lahir'=> Carbon::parse('1990-06-21'),
            ],
            [
                'nama' => 'Agus Suryodadi',
                'pekerjaan' => 'Guru',
                'tanggal_lahir'=> Carbon::parse('1992-06-06'),
            ],
            [
                'nama' => 'Ricky',
                'pekerjaan' => 'Dosen',
                'tanggal_lahir'=> Carbon::parse('1988-10-10'),
            ],
            [
                'nama' => 'Ayu kahitna',
                'pekerjaan' => 'Artis',
                'tanggal_lahir'=> Carbon::parse('2001-02-10'),
            ],
            [
                'nama' => 'Wulan',
                'pekerjaan' => 'Desainer',
                'tanggal_lahir'=> Carbon::parse('2002-10-20'),
            ],
        ];

        foreach($karyawan as $karyawans){
            Karyawan::create([
                'nama' => $karyawans['nama'],
                'pekerjaan' => $karyawans['pekerjaan'],
                'tanggal_lahir' => $karyawans['tanggal_lahir'],
            ]);
        }
    }
}

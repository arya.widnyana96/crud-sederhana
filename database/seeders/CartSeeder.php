<?php

namespace Database\Seeders;

use App\Models\Cart;
use Illuminate\Database\Seeder;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cart = [
            [
                'product' => 'Pulpen Snowman 230',
                'price' => 2500,
                'qty' => 2
            ],
            [
                'product' => 'Pensil 2B',
                'price' => 3000,
                'qty' => 1
            ],
            [
                'product' => 'Buku Tulis Sinar Dunia',
                'price' => 4000,
                'qty' => 3
            ]
        ];

        foreach ($cart as $carts) {
            Cart::create([
                'product' => $carts['product'],
                'price' => $carts['price'],
                'qty' => $carts['qty'],
            ]);
        }
    }
}

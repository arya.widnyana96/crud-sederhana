<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="m-4">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header text-center">
                    <h5>Halaman Utama</h5>
                </div>
                <div class="card-body">
                    <div class="text-center">
                        <label class="small" for="">Halaman ini merupakan halaman utama, silahkan plih halaman
                            yang telah disediakan</label>
                    </div>
                    <div class="d-flex align-items-center justify-content-center">
                        <div class="mt-2">
                            <a href="{{ route('karyawan.home') }}" class="btn btn-primary btn-sm mr-1 mb-1">
                                CRUD Blade
                            </a>
                            <a href="{{ route('cart') }}" class="btn btn-info btn-sm ml-1 mb-1">
                                Cart Page
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

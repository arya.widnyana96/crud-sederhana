<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
</head>

<body>
    <div class="m-4 d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5>Edit Data</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('karyawan.update', $karyawan->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" value="{{ $karyawan->nama }}" name="nama" id=""
                                placeholder="Input nama" required>
                        </div>
                        <div class="form-group">
                            <label for="">Pekerjaan</label>
                            <input type="text" class="form-control" value="{{ $karyawan->pekerjaan }}"
                                name="pekerjaan" id="" placeholder="Input pekerjaan" required>
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Lahir</label>
                            <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input class="form-control" value="{{ $karyawan->tanggal_lahir }}" type="text"
                                    name="tanggal_lahir" readonly="readonly" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script>
        $(".input-group.date").datepicker({
            autoclose: true,
            todayHighlight: true,
        });
    </script>
</body>

</html>

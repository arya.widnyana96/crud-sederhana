<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Detail {{ $karyawan->name }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="m-4 d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between">
                        <div class="card-title">Detail {{ $karyawan->nama }}</div>
                        <a href="/" class="btn btn-primary btn-sm">
                            Home
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="mb-1">
                            <label for="">Nama :</label>
                            <label for="">{{ $karyawan->nama }}</label>
                        </div>
                        <div class="mb-1">
                            <label for="">Pekerjaan :</label>
                            <label for="">{{ $karyawan->pekerjaan }}</label>
                        </div>
                        <div class="mb-1">
                            <label for="">Tanggal Lahir :</label>
                            <label for="">{{ $karyawan->tanggal_lahir }}</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>

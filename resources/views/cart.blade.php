<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Keranjang</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="m-4 d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex align-items-center justify-content-between">
                        <div class="card-title">Halaman Keranjang</div>
                        <a href="/" class="btn btn-success btn-sm">
                            Home
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                    $total = 0;
                                @endphp
                                @forelse ($cart as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->product }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ $item->price * $item->qty }}</td>
                                    </tr>
                                    @php($total += $item->price * $item->qty)
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="float-right">
                        <label for="">Total Bayar : {{ $total }}</label>
                        <div class="mt-1">
                            <a href='{{ route('payment') }}' class="btn btn-sm btn-primary">Checkout<a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Payment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="m-4 d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex align-items-center justify-content-between">
                        <div class="card-title">Halaman Payment</div>
                        <a href="/" class="btn btn-success btn-sm">
                            Home
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="container">
                        @php
                            $total = 0;
                        @endphp
                        @forelse ($cart as $item)
                            <div class="small mb-1">
                                <div class="row d-flex align-items-center justify-content-between">
                                    <p>Produk: {{ $item->product }} <span>({{ $item->qty }})</span></p>
                                    <div class="float-right">
                                        <label for="">{{ $item->price }}</label>
                                    </div>
                                </div>
                            </div>
                            @php($total += $item->price * $item->qty)
                        @empty
                        @endforelse
                        <div class="float-right mt-4">
                            <div class="small">
                                Bayar: {{ $total }}
                            </div>
                            <div class="mt-2">
                                <form action="{{ route('paymentStore') }}" method="POST">
                                    @csrf
                                    @foreach ($cart as $item)
                                        <input type="hidden" name="product[]" value='{{ $item->product }}' class="form-control">
                                        <input type="hidden" name="price[]" value="{{ $item->price }}" class="form-control">
                                        <input type="hidden" name="qty[]" value="{{ $item->qty }}" class="form-control">
                                    @endforeach
                                    <button class="btn btn-sm btn-primary">Payment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

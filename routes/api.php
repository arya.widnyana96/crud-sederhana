<?php

use App\Http\Controllers\KaryawanController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('karyawan', [KaryawanController::class, 'index']);
Route::get('karyawan/{id}', [KaryawanController::class, 'show']);
Route::get('karyawan/search', [KaryawanController::class, 'search']);
Route::post('karyawan/create',[KaryawanController::class, 'store']);
Route::put('karyawan/{id}/update', [KaryawanController::class, 'update']);
Route::delete('karyawan/{id}/delete', [KaryawanController::class, 'destroy']);

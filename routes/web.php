<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\DataTablesKaryawanController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KaryawanController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KaryawanFrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('karyawan-blade', [KaryawanFrontendController::class, 'index'])->name('karyawan.home');
Route::get('karyawan-blade/create', [KaryawanFrontendController::class, 'create'])->name('karyawan.create');
Route::post('karyawan-blade/store', [KaryawanFrontendController::class, 'store'])->name('karyawan.store');
Route::get('karyawan-blade/{id}/edit', [KaryawanFrontendController::class, 'edit'])->name('karyawan.edit');
Route::put('karyawan-blade/{id}/update', [KaryawanFrontendController::class, 'update'])->name('karyawan.update');
Route::delete('/karyawan-blade/{id}/delete', [KaryawanFrontendController::class, 'destroy'])->name('karyawan.delete');
Route::get('payment', [HomeController::class, 'payment'])->name('payment');
Route::get('karyawan-blade/{id}', [KaryawanFrontendController::class, 'show'])->name('karyawan.show');
Route::get('cart', [CartController::class, 'index'])->name('cart');
Route::post('paymentStore', [CartController::class, 'paymentStore'])->name('paymentStore');

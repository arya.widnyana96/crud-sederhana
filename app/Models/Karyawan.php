<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Karyawan extends Model
{
    use HasFactory, Uuids;
    protected $fillable = ['nama', 'pekerjaan', 'tanggal_lahir'];
}

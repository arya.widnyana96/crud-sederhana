<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;

class KaryawanFrontendController extends Controller
{
    public function index()
    {
        $karyawan = Karyawan::orderBy('created_at', 'DESC')->get();
        return view('karyawan.index', compact('karyawan'));
    }

    public function show($id)
    {
        $karyawan = Karyawan::find($id);
        if ($karyawan) {
            return view('karyawan.show', compact('karyawan'));
        }
    }

    public function create()
    {
        return view('karyawan.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|string',
            'pekerjaan' => 'required|string',
            'tanggal_lahir' => 'required',
        ]);
        Karyawan::create($validatedData);
        return redirect()->route('karyawan.home');
    }

    public function edit($id)
    {
        $karyawan = Karyawan::find($id);
        return view('karyawan.edit', compact('karyawan'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required|string',
            'pekerjaan' => 'required|string',
            'tanggal_lahir' => 'required',
        ]);
        $karyawan = Karyawan::find($id);
        if ($karyawan) {
            $karyawan->update($validatedData);
            return redirect()->route('karyawan.home');
        }
    }

    public function destroy($id)
    {
        $karyawan = Karyawan::find($id);
        if ($karyawan) {
            $karyawan->delete();
            return redirect()->route('karyawan.home');
        }
    }
}

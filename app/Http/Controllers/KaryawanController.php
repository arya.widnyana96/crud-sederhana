<?php

namespace App\Http\Controllers;

use App\Http\Requests\KaryawanRequest;
use App\Http\Resources\KaryawanResource;
use App\Models\Karyawan;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    public function index()
    {
        $karyawan = Karyawan::orderBy('created_at', 'DESC')->paginate();
        return KaryawanResource::collection($karyawan);
    }

    public function search(Request $request)
    {
        $karyawan = Karyawan::where('nama', 'LIKE', '%' . $request->search . '%')->get();
        return KaryawanResource::collection($karyawan);
    }

    public function create()
    {
        //
    }

    public function store(KaryawanRequest $request)
    {
        $karyawan = Karyawan::create([
            'nama' => $request->nama,
            'pekerjaan' => $request->pekerjaan,
            'tanggal_lahir' => $request->tanggal_lahir
        ]);
        if ($karyawan->exists) {
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil tersimpan',
                'data' => $karyawan
            ], 201);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data gagal tersimpan',
            ], 400);
        }
    }

    public function show($id)
    {
        $karyawan = Karyawan::find($id);
        if ($karyawan) {
            return new KaryawanResource($karyawan);
        } else {
            return response()->json([
                'status' => 'false',
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 500);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(KaryawanRequest $request, $id)
    {
        $karyawan = Karyawan::find($id);
        if ($karyawan) {
            $karyawan->update([
                'nama' => $request->nama,
                'pekerjaan' => $request->pekerjaan,
                'tanggal_lahir' => $request->tanggal_lahir,
            ]);
            if ($karyawan->exists) {
                return response()->json([
                    'status' => true,
                    'message' => 'Data berhasil diperbarui'
                ], 201);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal diperbarui'
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'false',
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 500);
        }
    }

    public function destroy($id)
    {
        $karyawan = Karyawan::find($id);
        if ($karyawan) {
            $karyawan->delete();
            return response()->json([
                'status' => true,
                'message' => 'Data berhasil dihapus'
            ], 201);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan'
            ], 500);
        }
    }
}

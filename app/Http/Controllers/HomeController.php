<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function payment()
    {
        $cart = Cart::all();
        return view('payment', compact('cart'));
    }
}

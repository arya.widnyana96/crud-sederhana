<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $cart = Cart::all();
        return view('cart', compact('cart'));
    }

    public function paymentStore(Request $request)
    {
        return response()->json([
            'success' => true,
            'data' => [
                'product' => $request->product,
                'price' => $request->price,
                'qty' => $request->qty
            ]
        ]);
    }
}
